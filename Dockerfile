ARG TELEGRAF_TAG
FROM telegraf:${TELEGRAF_TAG}

RUN apt-get update && apt-get install -y --no-install-recommends netcat && \
    rm -rf /var/lib/apt/lists/*

COPY ./scripts/piHoleCollector.sh /usr/bin/piHoleCollector.sh
