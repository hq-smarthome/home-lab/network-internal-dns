#!/bin/bash

FROM=$(date --date 'now - 2 minutes' +%s)
TO=$(date +%s)

RESULT=$(echo ">getallqueries-time ${FROM} ${TO}" | nc -q1 127.0.0.1 4711 | grep '^[0-9]' | awk 'NF')
RESULTS=$(echo "$RESULT" | awk 'NF' | wc -l)

if [[ "$RESULTS" != "0" ]]; then
  echo "$RESULT" | awk '{ print "nameResolution,type=" $2 ",domain=" $3 " action=" $5 " " $1 "000000000" }'
fi

exit 0
