# Network DNS

> :warning: **This project has been moved**
>
> Any further updates can be found here https://gitlab.com/carboncollins-cloud/network-internal-dns
> This repository will be archived in favour of all further development at the new location.


Pi hole DNS for users within hQ to have ads blocked at a network level

## CI/CD

This repository loads its CI/CD pipeline from a common template found in the
[Job Template](https://gitlab.com/hq-smarthome/home-lab/job-template) repository
